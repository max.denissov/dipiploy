plugins {
    id("com.android.library")
    kotlin("multiplatform") version "1.4.10"
    id("maven-publish")
}

group = "io.gitlab.denissov.dipiploy"
version = "1.7-SNAPSHOT"

repositories {
    maven { url = uri("https://dl.bintray.com/kotlin/kotlin-dev/") }
    google()
    jcenter()
    mavenCentral()
}

kotlin {
    android() {
        publishLibraryVariants("release", "debug")
    }
    jvm() {}
    js() {
        browser()
    }
    ios() {
        binaries {
            framework {
                baseName = "library"
            }
        }
    }
    watchos()
    linuxX64()
    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.google.android.material:material:1.2.1")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13")
            }
        }
        val iosMain by getting
        val iosTest by getting
        val watchosMain by getting
        val watchosTest by getting
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting
        val jsTest by getting
        val linuxX64Main by getting
        val linuxX64Test by getting
    }
}

android {
    compileSdkVersion(30)
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
    }
    sourceSets {
        getByName("main") {
            manifest.srcFile("src/androidMain/AndroidManifest.xml")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

//publishing {
//    repositories {
//        maven{
//            url = uri("$projectDir/dipiploy-repo")
//        }
//    }
//}

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/22777754/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
