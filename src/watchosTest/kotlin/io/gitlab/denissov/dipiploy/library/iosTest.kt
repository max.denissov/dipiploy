package io.gitlab.denissov.dipiploy.library

import kotlin.test.Test
import kotlin.test.assertTrue

class IosGreetingTest {

    @Test
    fun testExample() {
        assertTrue(Greeting().greeting().contains("Watchos"), "Check iOS is mentioned")
    }
}
