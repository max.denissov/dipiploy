package io.gitlab.denissov.dipiploy.library

expect class Platform() {
    val platform: String
}